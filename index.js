function sort() {
  var a = +document.getElementById("a").value;
  var b = +document.getElementById("b").value;
  var c = +document.getElementById("c").value;
  var result1;

  if (a < b && b < c ) {
    result1 = a + " < " + b + " < " + c;
  } else if (a < c && c < b) {
    result1 = a + " < " + c + " < " + b;
  } else if (b < a && a < c && b < c) {
    result1 = a + " < " + c + " < " + b;
  } else if (b < c && c < a && b < a) {
    result1 = b + " < " + c + " < " + a;
  } else if (c < a && a < b && c < b) {
    result1 = c + " < " + a + " < " + b;
  } else {
    result1 = c + " < " + b + " < " + a;
  }

  var notification1 = `<p>Sắp xếp: ${result1}</p>`;
  console.log(notification1);
  document.getElementById("notification1").innerHTML = notification1;
}

function sayHi() {
  var dad, mom, bro, sis;
  var value = document.getElementById("choice").value;

  if (value == "dad") {
    document.getElementById("notification2").innerHTML = `<p>Xin chào bố</p>`;
  } else if (value == "mom") {
    document.getElementById("notification2").innerHTML = `<p>Xin chào mẹ</p>`;
  } else if (value == "bro") {
    document.getElementById(
      "notification2"
    ).innerHTML = `<p>Xin chào anh trai</p>`;
  } else if (value == "sis") {
    document.getElementById(
      "notification2"
    ).innerHTML = `<p>Xin chào em gái</p>`;
  } else {
    document.getElementById(
      "notification2"
    ).innerHTML = `<p>Vui lòng chọn thành viên</p>`;
  }
}

function count() {
  var first = +document.getElementById("first").value;
  var second = +document.getElementById("second").value;
  var third = +document.getElementById("third").value;
  var count = 0;

  if (first % 2 == 0) {
    count++;
  }

  if (second % 2 == 0) {
    count++;
  }

  if (third % 2 == 0) {
    count++;
  }
  var notification3 = "Có " + count + " số chẵn, " + (3 - count) + " số lẻ";
  document.getElementById("notification3").innerHTML = notification3;
}

function test() {
  var first = +document.getElementById("first__edge").value;
  var second = +document.getElementById("second__edge").value;
  var third = +document.getElementById("third__edge").value;

  if (first == second && second == third) {
    document.getElementById(
      "notification4"
    ).innerHTML = `<p>Hình tam giác đều</p>`;
  } else if (first == second || second == third || first == third) {
    document.getElementById(
      "notification4"
    ).innerHTML = `<p>Hình tam giác cân</p>`;
  } else if (
    first == Math.sqrt(Math.pow(second, 2) + Math.pow(third, 2)) ||
    second == Math.sqrt(Math.pow(first, 2) + Math.pow(third, 2)) ||
    third == Math.sqrt(Math.pow(first, 2) + Math.pow(second, 2))
  ) {
    document.getElementById(
      "notification4"
    ).innerHTML = `<p>Hình tam giác vuông</p>`;
  } else {
    document.getElementById(
      "notification4"
    ).innerHTML = `<p>Hình tam giác khác</p>`;
  }
}

function yesterday() {
  var day = +document.getElementById("day").value;
  var month = +document.getElementById("month").value;
  var year = +document.getElementById("year").value;
  var result5 = day - 1 + "/" + month + "/" + year;
  var startMonth1 = "30/" + (month - 1) + "/" + year;
  var startMonth2 = "31/" + (month - 1) + "/" + year;


  if (day < 1 || day > 31 || month > 12) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (month % 2 == 0 && month <= 7 && day == 31) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (month % 2 == 1 && month > 7 && day == 31) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (month == 2 && day > 28) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (day == 1 && month == 1) {
    document.getElementById("notification5").innerHTML =
      `<p>31/12/</p>` + (year - 1);
  } else if (day == 1 && month == 3) {
    document.getElementById("notification5").innerHTML = `28/2/` + year;
  } else if (day == 1 && month % 2 == 1 && month <= 7) {
    document.getElementById("notification5").innerHTML = `${startMonth1}`;
  } else if (day == 1 && month % 2 == 0 && month > 7) {
    document.getElementById("notification5").innerHTML = `${startMonth2}`;
  } else {
    document.getElementById("notification5").innerHTML = `${result5}`;
  }
}

function tomorrow() {
  var day = +document.getElementById("day").value;
  var month = +document.getElementById("month").value;
  var year = +document.getElementById("year").value;
  var result5 = day + 1 + "/" + month + "/" + year;
  var endMonth = "1/" + (month + 1) + "/" + year;
 
  if (day < 1 || day > 31 || month > 12) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (month % 2 == 0 && month <= 7 && day == 31) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (month % 2 == 1 && month > 7 && day == 31) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (month == 2 && day > 28) {
    document.getElementById(
      "notification5"
    ).innerHTML = `<p>Ngày này không tồn tại</p>`;
  } else if (day == 31 && month == 12) {
    document.getElementById("notification5").innerHTML =
      `<p>1/1</p>` + (year + 1);
  } else if (day == 28 && month == 2) {
    document.getElementById("notification5").innerHTML = `1/3/` + year;
  } else if (day == 31 && month % 2 == 1 && month <= 7) {
    document.getElementById("notification5").innerHTML = `${endMonth}`;
  } else if (day == 31 && month % 2 == 0 && month > 7) {
    document.getElementById("notification5").innerHTML = `${endMonth}`;
  }  else if (day == 30 && month % 2 == 0 && month <= 6) {
    document.getElementById("notification5").innerHTML = `${endMonth}`;
  } else {
    document.getElementById("notification5").innerHTML = `${result5}`;
  }
}

function dayCount() {
  var month = +document.getElementById("monthSecond").value;
  var year = +document.getElementById("yearSecond").value;

  var leap = "Tháng 2 năm " + year + " có 29 ngày"; 
  var notLeap = "Tháng 2 năm " + year + " có 28 ngày"; 
  
  var month1 = "Tháng " + month + " năm " + year + " có 31 ngày";
  var month2 = "Tháng " + month + " năm " + year + " có 30 ngày";

  if(month == 2 && year % 4 == 0 && year % 100 != 0) {
    document.getElementById("notification6").innerHTML = `${leap}`;
  } else if(month == 2 && year % 400 == 0 ) {
    document.getElementById("notification6").innerHTML = `${leap}`;
  } else {
    document.getElementById("notification6").innerHTML = `${notLeap}`;
  }

  if(month <= 7 && month % 2 == 1) {
    document.getElementById("notification6").innerHTML = `${month1}`;
  } else if(month <= 7  && month % 2 == 0) {
    document.getElementById("notification6").innerHTML = `${month2}`;

  } else if(month > 7 && month % 2 == 0) {
    document.getElementById("notification6").innerHTML = `${month1}`;
  } else {
    document.getElementById("notification6").innerHTML = `${month2}`;
  }
}

function numberRead() {
  var number = +document.getElementById("number3").value;

  var units = (number % 100) % 10;
  console.log(units);
  var dozens = ((number - units) % 100) / 10;
  console.log(dozens);
  var hundreds = (number - dozens * 10 - units) / 100;
  console.log(hundreds);



  switch(hundreds) {
    case "1":{

    }
  }
}

function distance() {
  var name1 = document.getElementById("name1").value;
  var x1 = +document.getElementById("x1").value;
  var y1 = +document.getElementById("y1").value;
  
  var name2 = document.getElementById("name2").value;
  var x2 = +document.getElementById("x2").value;
  var y2 = +document.getElementById("y2").value;
  
  var name3 = document.getElementById("name3").value;
  var x3 = +document.getElementById("x3").value;
  var y3 = +document.getElementById("y3").value;
  
  var x4 = +document.getElementById("x4").value;
  var y4 = +document.getElementById("y4").value;

  var distance1 = Math.sqrt(Math.pow((x4 - x1), 2) + Math.pow((y4 - y1), 2));
  var distance2 = Math.sqrt(Math.pow((x4 - x2), 2) + Math.pow((y4 - y2), 2));
  var distance3 = Math.sqrt(Math.pow((x4 - x3), 2) + Math.pow((y4 - y3), 2));

  var result1 = "Sinh viên xa trường nhất: " + name1;
  var result2 = "Sinh viên xa trường nhất: " + name2;
  var result3 = "Sinh viên xa trường nhất: " + name3;

  if(distance1 > distance2 && distance1 > distance3) {
    document.getElementById("notification8").innerHTML = `${result1}`;
  } else if(distance2 > distance1 && distance2 > distance3) {
    document.getElementById("notification8").innerHTML = `${result2}`;
  } else {
    document.getElementById("notification8").innerHTML = `${result3}`;
  }
  
}
